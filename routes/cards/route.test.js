const mongoose = require("mongoose")
const { Card } = require("db/cards/collection")
const app = require("middleware/app")
const { responses } = require("db/cards/constants")
const config = require("config")
const User = require("db/users/collection")
const supertest = require("supertest")

const cardRouter = require("routes/cards/route")()
const jwtSecurity = require("core/jwt")
const jwt = require("jsonwebtoken")

const request = supertest(app)
const _ = require("underscore")
const { roles } = require("db/org-memberships/constants")

const userData = {
  profile: {
    firstName: "Michael",
    lastName: "Perju",
    birthdate: new Date(),
  },
  email: "admin@app.com",
  password: "danPot9_ij",
}

const data = {
  number: "4242424242424242",
  name: "John Doe",
  expiryMonth: "04",
  expiryYear: "23",
  cvc: "111",
}

const login = async (user) => {
  if (!user) user = await new User(userData).save()

  return jwt.sign(
    { email: user.email, userId: user?._id },
    config.jwt.secret,
    {
      expiresIn: config.jwt.expiresIn,
    },
  )
}

describe("organisations route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Card.deleteMany({})
    await User.deleteMany({})
  })

  it("creating a card with valid data and authentication should work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CARD_CREATED)
  })

  it("creating a card without authorization token should not work", async () => {
    app.use("/", cardRouter)

    const res = await request.post("/")
      .send({
        ...data,
      })
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.NO_TOKEN)
  })

  it("creating a card with an invalid authorization token should not work", async () => {
    app.use("/", cardRouter)

    const res = await request.post("/")
      .set("Authorization", "Bearer fakeTOKENhere")
      .send(data)
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.TOKEN_INVALID)
  })

  it("creating a card without a number should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        ...data,
        number: null,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_NUMBER)
  })

  it("creating a card with an invalid number should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        ...data,
        number: "424242424242424",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.INVALID_NUMBER)
  })

  it("creating a card with an invalid expiry month should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    for (const expiryMonth of [
      "-4",
      "-01",
      13,
      14,
      2020,
    ]) {
      const res = await request.post("/")
        .set("Authorization", `Bearer ${token}`)
        .send({
          ...data,
          expiryMonth,
        })
        .expect(400)

      expect(res.body.message).toBe(responses.INVALID_MONTH)
    }
  })

  it("creating a card without an expiry month should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        ...data,
        expiryMonth: null,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_MONTH)
  })

  it("creating a card with an invalid expiry year should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    for (const expiryYear of ["-2", "00", "12", "20", "51", 60, 90, -12323, 2020, 2000, 2051, 2063]) {
      const res = await request.post("/")
        .set("Authorization", `Bearer ${token}`)
        .send({
          ...data,
          expiryYear,
        })
        .expect(400)

      expect(res.body.message).toBe(responses.INVALID_YEAR)
    }
  })

  it("creating a card without an expiry year should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        ...data,
        expiryYear: null,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_YEAR)
  })

  it("creating a card without a cvc should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        ...data,
        cvc: null,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CVC)
  })

  it("creating a card with an invalid cvc should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    for (const cvc of ["-2", "00", "12", "20", "51", "60", "90", "-12323", "20200", "10000", "20511", "29063"]) {
      const res = await request.post("/")
        .set("Authorization", `Bearer ${token}`)
        .send({
          ...data,
          cvc,
        })
        .expect(400)

      expect(res.body.message).toBe(responses.INVALID_CVC)
    }
  })

  it("creating a card without a name should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    const res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        ...data,
        name: null,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_NAME)
  })

  it("creating a card twice should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    // a card that has the same number and expiry dates are not
    let res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CARD_CREATED)

    res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        ...data,
        name: "Michael Doe",
        cvc: "454",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.CARD_ALREADY_CREATED)
  })
})
