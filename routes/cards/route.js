const mongoose = require("mongoose")
const express = require("express")
const errorService = require("lib/errors/handler")

const { Card } = require("db/cards/collection")
const jwtSecurity = require("core/jwt")
const { responses } = require("db/cards/constants")

const _ = require("underscore")
const routeSecurity = require("./security")
// const middleware = require("./middleware")

const createRouter = () => {
  const cardRouter = express.Router()

  cardRouter.post("/", [
    jwtSecurity.checkToken,
    routeSecurity.create,
  ], async (req, res, next) => {
    try {
      const userId = req?.decoded?.userId
      const data = req?.body
      _.extend(data, { userId })

      const card = await new Card(data).save()

      return res.json({ message: responses.CARD_CREATED, card })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on cards / route",
      })
    }
  })

  cardRouter.get("/me", [
    jwtSecurity.checkToken,
    routeSecurity.me,
  ], async (req, res, next) => {
    try {
      const userId = req?.decoded?.userId

      const cards = await Card.aggregate([
        {
          $match: {
            userId: mongoose.Types.ObjectId(userId),
          },
        },
        {
          $project: {
            number: { $substr: ["$number", 12, 16] },
          },
        },
      ])
      const total = await Card.countDocuments({ userId })

      return res.json({ cards, total })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on cards /me route",
      })
    }
  })

  cardRouter.delete("/", [
    jwtSecurity.checkToken,
    routeSecurity.remove,
  ], async (req, res, next) => {
    try {
      const cardId = req?.body?.cardId

      await Card.deleteOne({ _id: cardId })

      return res.json({ message: responses.CARD_REMOVED })
    } catch (error) {
      return errorService.handle({
        req, res, next, message: "Error on cards / delete route",
      })
    }
  })

  return cardRouter
}

module.exports = createRouter
