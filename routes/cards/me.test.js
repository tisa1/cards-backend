const mongoose = require("mongoose")
const { Card } = require("db/cards/collection")
const app = require("middleware/app")
const { responses } = require("db/cards/constants")
const config = require("config")
const User = require("db/users/collection")
const supertest = require("supertest")

const cardRouter = require("routes/cards/route")()
const jwtSecurity = require("core/jwt")
const jwt = require("jsonwebtoken")

const request = supertest(app)
const _ = require("underscore")
const { roles } = require("db/org-memberships/constants")

const userData = {
  profile: {
    firstName: "Michael",
    lastName: "Perju",
    birthdate: new Date(),
  },
  email: "admin@app.com",
  password: "danPot9_ij",
}

const data = {
  number: "4242424242424242",
  name: "John Doe",
  expiryMonth: "04",
  expiryYear: "23",
  cvc: "111",
}

const login = async (user) => {
  if (!user) user = await new User(userData).save()

  return jwt.sign(
    { email: user.email, userId: user?._id },
    config.jwt.secret,
    {
      expiresIn: config.jwt.expiresIn,
    },
  )
}

describe("organisations route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Card.deleteMany({})
    await User.deleteMany({})
  })

  it("fetching user's cards with valid data and authentication should work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    let res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CARD_CREATED)
    expect(res.body.message).not.toBe(undefined)

    res = await request.get("/me")
      .set("Authorization", `Bearer ${token}`)
      .expect(200)

    expect(res.body.total).toBe(1)
    expect(res.body.cards.length).toBe(1)
  })

  it("fething user's cards without authorization token should not work", async () => {
    app.use("/", cardRouter)

    const res = await request.delete("/")
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.NO_TOKEN)
    expect(res.body.message).not.toBe(undefined)
  })

  it("fetching user's cards with an invalid authorization token should not work", async () => {
    app.use("/", cardRouter)

    const res = await request.get("/me")
      .set("Authorization", "Bearer fakeTOKENhere")
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.TOKEN_INVALID)
    expect(res.body.message).not.toBe(undefined)
  })

  it("fetching user's cards should only return their cards", async () => {
    app.use("/", cardRouter)

    const user1 = await new User(userData).save()
    const user2 = await new User({
      ...userData, email: "user@app.com",
    }).save()
    const token1 = await login(user1)
    const token2 = await login(user2)

    // creating the first card
    let res = await request.post("/")
      .set("Authorization", `Bearer ${token1}`)
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CARD_CREATED)
    expect(res.body.message).not.toBe(undefined)

    // creating another card
    res = await request.post("/")
      .set("Authorization", `Bearer ${token2}`)
      .send({
        ...data,
        number: "4000002760003184",
      })
      .expect(200)

    // testing that the cards got created
    const cardsNo = await Card.countDocuments()
    expect(cardsNo).toBe(2)

    res = await request.get("/me")
      .set("Authorization", `Bearer ${token1}`)
      .expect(200)

    expect(res.body.total).toBe(1)
    expect(res.body.cards.length).toBe(1)

    res = await request.get("/me")
      .set("Authorization", `Bearer ${token2}`)
      .expect(200)

    expect(res.body.total).toBe(1)
    expect(res.body.cards.length).toBe(1)
  })

  it("fetching user's cards should return only 4 digits from the card", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)

    let res = await request.post("/")
      .set("Authorization", `Bearer ${token}`)
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CARD_CREATED)
    expect(res.body.message).not.toBe(undefined)

    res = await request.get("/me")
      .set("Authorization", `Bearer ${token}`)
      .expect(200)

    expect(res.body.total).toBe(1)
    expect(res.body.cards.length).toBe(1)

    // expecting the card number to return only 4 digits
    expect(res.body.cards[0].number.length).toBe(4)
  })
})
