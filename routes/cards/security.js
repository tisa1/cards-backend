const mongoose = require("mongoose")
const User = require("db/users/collection")
const { Card } = require("db/cards/collection")
const { responses } = require("db/cards/constants")
const { isCreditCard } = require("validator")

const create = async (req, res, next) => {
  const userId = req?.decoded?.userId

  const number = req?.body.number
  if (!number) {
    return res.status(400).json({ message: responses.NO_NUMBER })
  }

  if (!isCreditCard(number)) {
    return res.status(400).json({ message: responses.INVALID_NUMBER })
  }

  const expiryMonth = req?.body.expiryMonth
  if (!expiryMonth) {
    return res.status(400).json({ message: responses.NO_MONTH })
  }

  if (Number.isNaN(Number(expiryMonth)) || Number(expiryMonth) > 11 || Number(expiryMonth) < 0) {
    return res.status(400).json({ message: responses.INVALID_MONTH })
  }

  const expiryYear = req?.body.expiryYear
  if (!expiryYear) {
    return res.status(400).json({ message: responses.NO_YEAR })
  }

  if (Number.isNaN(Number(expiryYear))) {
    return res.status(400).json({ message: responses.INVALID_YEAR })
  }

  const yearUpperLimit = Number(expiryYear) < 100 ? 50 : 2050
  const yearLowerLimit = Number(expiryYear) < 100 ? 21 : 2021
  if (Number(expiryYear) > yearUpperLimit || Number(expiryYear) < yearLowerLimit) {
    return res.status(400).json({ message: responses.INVALID_YEAR })
  }

  const cvc = req?.body.cvc
  if (!cvc) {
    return res.status(400).json({ message: responses.NO_CVC })
  }

  if (cvc.length > 4 || cvc.length < 3) {
    return res.status(400).json({ message: responses.INVALID_CVC })
  }

  if (Number.isNaN(Number(cvc))) {
    return res.status(400).json({ message: responses.INVALID_CVC })
  }

  const name = req?.body?.name
  if (!name) {
    return res.status(400).json({ message: responses.NO_NAME })
  }

  const existingCard = await Card.findOne({ number, expiryMonth, expiryYear })
  if (existingCard) {
    return res.status(400).json({ message: responses.CARD_ALREADY_CREATED })
  }

  return next()
}

const me = async (req, res, next) => next()

const remove = async (req, res, next) => {
  const userId = req?.decoded?.userId
  const cardId = req?.body?.cardId
  if (!cardId) {
    return res.status(400).json({ message: responses.NO_CARD_ID })
  }

  if (!mongoose.Types.ObjectId.isValid(cardId)) {
    return res.status(400).json({ message: responses.CARD_ID_INVALID })
  }

  const existingCard = await Card.findOne({ _id: cardId, userId }, { _id: 1 })
  if (!existingCard) {
    return res.status(400).json({ message: responses.NO_CARD })
  }

  return next()
}
module.exports = {
  create,
  me,
  remove,
}
