const mongoose = require("mongoose")
const { Card } = require("db/cards/collection")
const app = require("middleware/app")
const { responses } = require("db/cards/constants")
const config = require("config")
const User = require("db/users/collection")
const supertest = require("supertest")

const cardRouter = require("routes/cards/route")()
const jwtSecurity = require("core/jwt")
const jwt = require("jsonwebtoken")

const request = supertest(app)
const _ = require("underscore")
const { roles } = require("db/org-memberships/constants")

const userData = {
  profile: {
    firstName: "Michael",
    lastName: "Perju",
    birthdate: new Date(),
  },
  email: "admin@app.com",
  password: "danPot9_ij",
}

const data = {
  number: "4242424242424242",
  name: "John Doe",
  expiryMonth: "04",
  expiryYear: "23",
  cvc: "111",
}

const login = async (user) => {
  if (!user) user = await new User(userData).save()

  return jwt.sign(
    { email: user.email, userId: user?._id },
    config.jwt.secret,
    {
      expiresIn: config.jwt.expiresIn,
    },
  )
}

const createCard = async (user) => {
  if (!user) user = await new User(userData).save()

  _.extend(data, { userId: user._id })
  return new Card(data).save()
}

describe("organisations route", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Card.deleteMany({})
    await User.deleteMany({})
  })

  it("deleting a card with valid data and authentication should work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)
    const card = await createCard(user)

    const res = await request.delete("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        cardId: card._id,
      })
      .expect(200)

    expect(res.body.message).toBe(responses.CARD_REMOVED)
    expect(res.body.message).not.toBe(undefined)

    const cardsNo = await Card.countDocuments()
    expect(cardsNo).toBe(0)
  })

  it("deleting a card without authorization token should not work", async () => {
    app.use("/", cardRouter)

    const card = await createCard()
    const res = await request.delete("/")
      .send({
        cardId: card._id,
      })
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.NO_TOKEN)
    expect(res.body.message).not.toBe(undefined)
  })

  it("deleting a card with an invalid authorization token should not work", async () => {
    app.use("/", cardRouter)

    const card = await createCard()
    const res = await request.delete("/")
      .set("Authorization", "Bearer fakeTOKENhere")
      .send({
        cardId: card._id,
      })
      .expect(403)

    expect(res.body.message).toBe(jwtSecurity.responses.TOKEN_INVALID)
    expect(res.body.message).not.toBe(undefined)
  })

  it("deleting a card without a card id should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)
    await createCard(user)

    const res = await request.delete("/")
      .set("Authorization", `Bearer ${token}`)
      .send()
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CARD_ID)
    expect(res.body.message).not.toBe(undefined)
  })

  it("deleting a card with an invalid card id should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)
    await createCard(user)

    const res = await request.delete("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        cardId: "fakeId",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.CARD_ID_INVALID)
    expect(res.body.message).not.toBe(undefined)
  })

  it("deleting a card with an unexisting card id should not work", async () => {
    app.use("/", cardRouter)

    const user = await new User(userData).save()
    const token = await login(user)
    await createCard(user)

    const res = await request.delete("/")
      .set("Authorization", `Bearer ${token}`)
      .send({
        cardId: "5f6c9a72a7985000298c24bc",
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CARD)
    expect(res.body.message).not.toBe(undefined)
  })

  it("deleting a card with an unexisting card id should not work", async () => {
    app.use("/", cardRouter)

    const user1 = await new User(userData).save()
    const user2 = await new User({ ...userData, email: "user@app.com" }).save()
    const token1 = await login(user1)
    const token2 = await login(user2)

    let res = await request.post("/")
      .set("Authorization", `Bearer ${token1}`)
      .send(data)
      .expect(200)

    expect(res.body.message).toBe(responses.CARD_CREATED)
    expect(res.body.message).not.toBe(undefined)

    const { card } = res.body

    res = await request.delete("/")
      .set("Authorization", `Bearer ${token2}`)
      .send({
        cardId: card._id,
      })
      .expect(400)

    expect(res.body.message).toBe(responses.NO_CARD)
    expect(res.body.message).not.toBe(undefined)

    const cardsNo = await Card.countDocuments()
    expect(cardsNo).toBe(1)
  })
})
