const mongoose = require("mongoose")
const logger = require("core/logger")

const app = require("./middleware/app")

require("./middleware/http")
require("./middleware/sockets")

// ROUTERS
const metaRouter = require("./routes/meta/route")()
const cardRouter = require("./routes/cards/route")()

const init = require("init")

// CONFIGURATION
const config = require("./config.js")

const start = async () => {
  try {
    await init()

    // DATABASE CONNECTION
    mongoose.connect(config.mongo.uri, config.mongo.options)

    // When successfully connected
    mongoose.connection.on("connected", () => {
      const db = mongoose.connection

      db.once("open", () => {
        app.use("/", cardRouter)
        app.use("/meta", metaRouter)
      })

      // Mongoose Connection Error
      db.on("error", (err) => {
        logger.error({ message: "MongoDb connection has died. We did everything we could", data: err })
      })
    })

    mongoose.connection.on("error", (err) => {
      logger.error({ message: "MongoDB is not ready for a serious relationship", data: err })
    })
  } catch (error) {
    logger.error({ message: error.message, data: error })
  }
}

start()
