const mongoose = require("mongoose")

// Mongoose Schema for Users
const UserSchema = new mongoose.Schema({
  profile: {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
  },
}, { timestamps: true })

module.exports = {
  UserSchema,
}
