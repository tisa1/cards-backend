const responses = {
  CARD_CREATED: "cardCreated",
  CARD_REMOVED: "cardRemoved",

  NO_NUMBER: "noNumber",
  INVALID_NUMBER: "invalidNumber",

  NO_MONTH: "noMonth",
  INVALID_MONTH: "invalidMonth",

  NO_YEAR: "noYear",
  INVALID_YEAR: "invalidYear",

  NO_CVC: "noCvc",
  INVALID_CVC: "invalidCvc",

  NO_NAME: "noName",

  CARD_ALREADY_CREATED: "cardAlreadyCreated",

  NO_CARD_ID: "noCardId",
  CARD_ID_INVALID: "cardIdInvalid",
  NO_CARD: "noCard",
}

module.exports = {
  responses,
}
