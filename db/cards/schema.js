const mongoose = require("mongoose")
const { isCreditCard, isInt } = require("validator")

// Mongoose Schema for Orgs
const CardSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  number: {
    type: String,
    validate: [isCreditCard, "Not a valid card number"],
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  expiryMonth: {
    type: String,
    required: true,
    validate: {
      validator(v) {
        return !Number.isNaN(Number(v)) && Number(v) < 13 && Number(v) >= 0
      },
      message: (props) => `${props.value} is not a valid month!`,
    },
  },
  expiryYear: {
    type: String,
    required: true,
    validate: {
      validator(v) {
        const upperLimit = v < 100 ? 50 : 2050
        const lowerLimit = v < 100 ? 21 : 2021
        return !Number.isNaN(Number(v)) && Number(v) <= upperLimit && Number(v) >= lowerLimit
      },
      message: (props) => `${props.value} is not a valid year!`,
    },
  },
  cvc: {
    type: String,
    required: true,
    validate: {
      validator(v) {
        return typeof v === "string" && (v?.length < 5 && v?.length > 2)
        && !Number.isNaN(Number(v))
      },
      message: (props) => `${props.value} is not a valid CVC!`,
    },
  },
}, { timestamps: true })

module.exports = {
  CardSchema,
}
