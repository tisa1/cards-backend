const mongoose = require("mongoose")
const { Card } = require("db/cards/collection")

describe("organisations", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await Card.deleteMany({})
  })

  it("should create a card", async () => {
    const data = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      number: "4242424242424242",
      name: "John Doe",
      expiryMonth: "04",
      expiryYear: "23",
      cvc: "111",
    }

    await new Card(data).save()
    const num = await Card.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you create a card with an invalid number", async () => {
    const fakeCard = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      number: "424242424242424", // invalid number
      name: "John Doe",
      expiryMonth: "04",
      expiryYear: "23",
      cvc: "111",
    }

    let error = null
    try {
      await new Card(fakeCard).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })

  it("should NOT let you create a card with an invalid expiry month", async () => {
    const fakeCard = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      number: "4242424242424242",
      name: "John Doe",
      expiryYear: "23",
      cvc: "111",
    }

    for (const expiryMonth of ["-4", "-01", 13, 14, 2020]) {
      let error = null
      try {
        await new Card({
          ...fakeCard,
          expiryMonth,
        }).save()
      } catch (e) {
        error = e
      }

      expect(error).not.toBeNull()
    }
  })

  it("should NOT let you create a card with an invalid expiry year", async () => {
    const fakeCard = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      number: "4242424242424242",
      name: "John Doe",
      expiryMonth: "04",
      cvc: "111",
    }

    for (const expiryYear of ["-2", "00", "12", "20", "51", 60, 90, -12323, 2020, 2000, 2051, 2063]) {
      let error = null
      try {
        await new Card({
          ...fakeCard,
          expiryYear,
        }).save()
      } catch (e) {
        error = e
      }

      expect(error).not.toBeNull()
    }
  })
  it("should NOT let you create a card with an invalid cvc", async () => {
    const fakeCard = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      number: "4242424242424242",
      name: "John Doe",
      expiryMonth: "04",
      expiryYear: "2025",
    }

    for (const cvc of ["-2", "00", "12", "20", "51", "60", "90", "-12323", "20200", "10000", "20511", "29063"]) {
      let error = null
      try {
        await new Card({
          ...fakeCard,
          cvc,
        }).save()
      } catch (e) {
        error = e
      }

      expect(error).not.toBeNull()
    }
  })

  it("should NOT let you insert an empty user", async () => {
    const fakeCard = {}
    let error = null
    try {
      await new Card(fakeCard).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
