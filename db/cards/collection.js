const mongoose = require("mongoose")
const { CardSchema } = require("./schema")

const Card = mongoose.model("cards", CardSchema)

module.exports = {
  Card,
}
