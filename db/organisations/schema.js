const mongoose = require("mongoose")

// Mongoose Schema for Orgs
const OrgSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
}, { timestamps: true })

module.exports = {
  OrgSchema,
}
