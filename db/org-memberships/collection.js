const mongoose = require("mongoose")
const { OrgMembershipSchema } = require("./schema")

const OrgMembership = mongoose.model("org-memberships", OrgMembershipSchema)


module.exports = { OrgMembership }
