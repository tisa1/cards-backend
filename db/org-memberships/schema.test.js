const mongoose = require("mongoose")
const { OrgMembership } = require("db/org-memberships/collection")
const { roles } = require("db/org-memberships/constants")

describe("org-memberships", () => {
  beforeAll(async () => {
    await mongoose.connect(global.__MONGO_URI__, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })

  beforeEach(async () => {
    await OrgMembership.deleteMany({})
  })

  it("should create an org memberships", async () => {
    const data = {
      userId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
      role: roles.OWNER,
      orgId: mongoose.Types.ObjectId("5f6c9a72a7985000298c24bc"),
    }

    await new OrgMembership(data).save()
    const num = await OrgMembership.countDocuments()
    expect(num).toBe(1)
  })

  it("should NOT let you insert an empty org memberships", async () => {
    const fakeOrgMembership = {}
    let error = null
    try {
      await new OrgMembership(fakeOrgMembership).save()
    } catch (e) {
      error = e
    }

    expect(error).not.toBeNull()
  })
})
