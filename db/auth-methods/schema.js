const mongoose = require("mongoose")
const bcrypt = require("bcryptjs")

const _ = require("underscore")
const { isEmail } = require("validator")
const { authMethodTypes } = require("./constants")

const allowedAuthMethodTypes = _.map(authMethodTypes, (a) => a)

// Mongoose Schema for Auth
const AuthSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  type: {
    type: String,
    enum: allowedAuthMethodTypes,
  },
  credentials: {
    email: {
      type: String,
      validate: [isEmail, "Not a valid email"],
      lowercase: true,
      trim: true,
      required() {
        return this.type === authMethodTypes.PASSWORD
      },
    },
    password: {
      type: String,
      required() {
        return this.type === authMethodTypes.PASSWORD
      },
    },
  },
},
{ timestamps: true })

AuthSchema.methods = {
  checkPassword(inputPassword) {
    return bcrypt.compareSync(inputPassword, this.password)
  },
  hashPassword: (plainTextPassword) => bcrypt.hashSync(plainTextPassword, 10),
}

// Define pre-hooks for the save method
AuthSchema.pre("save", function (next) { // eslint-disable-line
  if (!this.credentials?.password) {
    next()
  } else {
    this.credentials.password = this.hashPassword(this.credentials.password)
    next()
  }
})

const PasswordAuthSchema = new mongoose.Schema({
  method: {
    type: String,
    default: authMethodTypes.PASSWORD,
  },
})

module.exports = {
  AuthSchema,
  PasswordAuthSchema,
}
