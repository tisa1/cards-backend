module.exports = {
  preset: "@shelf/jest-mongodb",
  modulePaths: [
    ".",
  ],
  moduleFileExtensions: [
    "js",
    "jsx",
    "json",
    "node",
  ],
  transformIgnorePatterns: [
    "/node_modules/(?!@techcityventures).+\\.js$",
  ],
  transform: {
    "^.+\\.jsx?$": "babel-jest",
  },
  globals: {
    "jest-mongodb": {
      babelConfig: true,
    },
  },
}
